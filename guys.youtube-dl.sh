#define variables

youtubedl_runtime_path="/home/neon/youtube-dl-runtime1/youtube-dl/youtube-dl"
youtubedl_output_path="/home/neon/youtube-dl/downloads/"


extract_audio="-x"
date_after="--dateafter 20200904"
preferred_formats="-f bestaudio/worstvideo"
#verbose="--verbose"
verbose=""
ignore_errors="-i"
no_overwrites="-w"
continue_incomplete="-c"
download_archive="--download-archive archive.txt"
output_path="-o $youtubedl_output_path%(channel_id)s.%(upload_date)s.%(uploader)s.%(title)s.%(ext)s"
params="$extract_audio $verbose $ignore_errors $no_overwrites $continue_incomplete $download_archive $date_after $preferred_formats $output_path"
sleep_time=5
threshold_speed=800



# channel root pages, one per line, no commas
channel_paths=(
"https://www.youtube.com/user/LinusTechTips"
)

# execute script

#create download folder if it doesn't exist
mkdir -p $youtubedl_output_path

len=${#channel_paths[@]}



i=0
while (($i < $len)); do
    $youtubedl_runtime_path $params ${channel_paths[$i]} >/dev/null &
    ((i++))
    
    while : ; do
#        sleep $sleep_time
    
        download_speed_string="$(ifstat -T 10 1)" 
        download_speed_array=(${download_speed_string// / })
        lend=${#download_speed_array[@]}
        download_speed=${download_speed_array[((lend - 2))]}

        download_speed=${download_speed%.*}
        echo -e "\n=> Current download speed is [$download_speed KB/s]"
                
        if (( $download_speed<$threshold_speed ));
        then
            echo "=> Going to next channel"
            break
        else
            echo "=> Waiting because download speed is too high"
        fi
    done
done
